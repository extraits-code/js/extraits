//##Une fermeture (closure) en JavaScript est une fonction qui a été déclarée dans une autre fonction et qui conserve l'accès aux variables de la fonction externe, même après que cette dernière ait terminé son exécution. Voici un exemple simple :

function externe() {
	var message = 'Bonjour';

	function interne() {
		console.log(message);
	}

	return interne;
}

var fermeture = externe();
fermeture(); // affichera "Bonjour"

//??Dans cet exemple, la fonction interne est déclarée à l'intérieur de la fonction externe. Même après que externe ait terminé son exécution, la fonction interne conserve l'accès à la variable message définie dans externe. Les closures sont souvent utilisées pour créer des variables privées, implémenter des modules, et pour créer des fonctions de rappel (callback functions) en JavaScript.
